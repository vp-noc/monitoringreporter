# MonitoringReporter
[![License](https://img.shields.io/badge/license-Apache-blue.svg)](LICENSE)
[![build status](https://gitlab.com/vp-noc/monitoringreporter/badges/develop/build.svg)](https://gitlab.com/vp-noc/monitoringreporter/commits/develop)
[![coverage report](https://gitlab.com/vp-noc/monitoringreporter/badges/develop/coverage.svg)](https://vp-noc.gitlab.io/monitoringreporter/)
[![Gem Version](https://badge.fury.io/rb/monitoringreporter.svg)](https://badge.fury.io/rb/monitoringreporter)

1. [Overview](#overview)
2. [Description](#role-description)
3. [Setup](#setup)
4. [Usage](#usage)
5. [Limitations](#limitations)
6. [Development](#development)
7. [Miscellaneous](#miscellaneous)

## Overview

`MonitoringReporter` is a simple tool to request monitoring tool data base and
extract some information useful to be reported.

## Description

This tool request the reference tool containing the datas to extract and format
useful information into a simple report message.

## Setup

    $ gem install monitoringreporter

## Usage

An interactive help is available with:

    $ monitoringreporter help

## Examples

To generate a simple report from a database:

    $ monitoringreporter report -h "<DB_HOST>" -u "<USER>" -p "<PASSWORD>" -d "<DB>"

## Limitations

It is currently only available to request LibreNMS.

## Development

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

```
    ╚⊙ ⊙╝
  ╚═(███)═╝
 ╚═(███)═╝
╚═(███)═╝
 ╚═(███)═╝
  ╚═(███)═╝
   ╚═(███)═╝
```
