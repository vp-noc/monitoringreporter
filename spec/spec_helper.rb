#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
require 'simplecov'
SimpleCov.start

$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'monitoringreporter'

# Force expect syntax over should
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

dir = File.dirname(__FILE__)
# Load helpers
Dir[File.join(dir, 'helpers', '*.rb')].each { |file| require file }

def now
  Time.new.strftime('%Y-%m-%dT%H:%M:%S%z').insert(-3, ':')
end

# To test the cli
def start(caller_object, subcommand = nil)
  cmd = caller_object.class.description.split(' ')
  args = [subcommand, *cmd].compact
  MonitoringReporter.start(args)
end
