#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

def read_file(file)
  dir = File.dirname(__FILE__)
  File.read(File.join(dir, '..', 'files', file))
end

describe MonitoringReporter::CLI do # rubocop:disable Metrics/BlockLength
  outfile = '/tmp/out'
  opth = '-h mysql'
  optu = '-u testuser'
  optp = '-p testpasswd'
  optd = '-d test'
  optt = '-t 1501106400'
  opto = "-o #{outfile}"
  optf = '-f json'

  context "report #{opth} #{optu} #{optp} #{optd} #{optt}" do
    it 'reports from the fake database.' do
      expect { start(self) }.to output(read_file('output.text')).to_stdout
    end
  end

  context "report #{opth} #{optu} #{optp} #{optd} #{optt} #{opto}" do
    it 'reports from the fake database and writes to file.' do
      expect { start(self) }.to output('').to_stdout
      expect { print File.read(outfile) }
        .to output(read_file('output.text')).to_stdout
      File.delete(outfile)
    end
  end

  context "report #{opth} #{optu} #{optp} #{optd} #{optt} #{optf}" do
    it 'reports from the fake database with json format.' do
      expect { start(self) }.to output(read_file('output.json')).to_stdout
    end
  end

  context "report -h error #{optu} #{optp} #{optd} #{optt}" do
    it 'does not report from an unreachable database.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context 'notexistingcmd' do
    it 'is an unknown command and does nothing.' do
      out = "Could not find command \"notexistingcmd\".\n"
      expect { start(self) }.to output(out).to_stderr
    end
  end
end
