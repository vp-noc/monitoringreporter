#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'monitoringreporter/version'

dev_deps = {
  'bundler' => '~> 1.12',
  'rspec' => '~> 3.5',
  'rake' => '~> 11.2',
  'rubocop' => '~> 0.41',
  'simplecov' => '~> 0.12'
}

deps = {
  'ruby-mysql' => '~> 2.9',
  'thor' => '~> 0.19'
}

Gem::Specification.new do |s|
  s.name = 'monitoringreporter'
  s.version = MonitoringReporter::VERSION
  s.authors = ['Richard Delaplace']
  s.email = 'rdelaplace@vente-privee.com'
  s.license = 'Apache-2.0'

  s.summary = 'Tool to request monitoring tool database.'
  s.description = 'MonitoringReporter is a simple tool to request monitoring' \
    ' tool data base and extract some information useful to be reported'
  s.homepage = 'https://github.com/vp-noc/monitoringreporter'

  s.files = `git ls-files`.lines.map(&:chomp)
  s.bindir = 'bin'
  s.executables = `git ls-files bin/*`.lines.map do |exe|
    File.basename(exe.chomp)
  end
  s.require_paths = ['lib']

  s.required_ruby_version = '>= 1.9.3'

  dev_deps.each_pair do |deps_gem, deps_version|
    s.add_development_dependency deps_gem, deps_version
  end

  deps.each_pair do |deps_gem, deps_version|
    s.add_dependency deps_gem, deps_version
  end
end
