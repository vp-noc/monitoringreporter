#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'mysql'

module MonitoringReporter
  # This is mysql class to request the databse.
  class MySQL
    # Class constructor method
    def initialize(opts)
      @opts = opts
      @my = Mysql.connect(
        opts['host'],
        opts['user'],
        opts['password'],
        opts['database'],
        opts['port'],
        opts['socket']
      )
    end

    def query(query)
      @my.query(query).to_a
    end
  end
end
