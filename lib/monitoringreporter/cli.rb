#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'

module MonitoringReporter
  # Simple CLI for monitoringreporter
  class CLI < Thor
    desc 'version', 'Print monitoringreporter current version'
    def version
      puts "MonitoringReporter version #{MonitoringReporter::VERSION}"
    end

    desc('report [options]',
         'Send a request to the DB to get the requested data.' \
         '  DATA: may be one of []')
    option(
      :baseurl,
      aliases: ['-b'],
      desc: 'Monitoring tool base url.'
    )
    option(
      :host,
      aliases: ['-h'],
      desc: 'Database host to request.'
    )
    option(
      :port,
      aliases: ['-P'],
      default: '3306',
      desc: 'Database port to request.'
    )
    option(
      :socket,
      aliases: ['-x'],
      desc: 'Database socket to request.'
    )
    option(
      :database,
      aliases: ['-d'],
      desc: 'Database to request.'
    )
    option(
      :user,
      aliases: ['-u'],
      desc: 'User name in order to login. See also password option.'
    )
    option(
      :password,
      aliases: ['-p'],
      desc: 'User password in order to login. See also user option.'
    )
    option(
      :output,
      aliases: ['-o'],
      desc: 'Output file.'
    )
    option(
      :periodicity,
      aliases: ['-t'],
      type: :numeric,
      default: 43_200,
      desc: 'Report on the last T times.'
    )
    option(
      :source,
      aliases: ['-S'],
      default: 'librenms',
      enum: %w[librenms],
      desc: 'Select the source db type.'
    )
    option(
      :format,
      aliases: ['-f'],
      default: 'text',
      enum: %w[text json],
      desc: 'Select the output format type.'
    )
    option(
      :quiet,
      aliases: ['-q'],
      type: :boolean,
      default: false,
      desc: 'Silently do the job'
    )
    option(
      :simulation,
      aliases: ['-s'],
      type: :boolean,
      default: false,
      desc: 'Simulation mode. Do nothing'
    )
    def report
      opts = options.dup
      rep = MonitoringReporter::Reporter.new.send(opts['source'], opts) \
        unless opts['simulation']
      rep.report unless opts['simulation']
    end
  end
end
