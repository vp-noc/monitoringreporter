#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'monitoringreporter/mysql'
require 'json'

module MonitoringReporter
  # This is librenms class to request the various datas.
  class MysqlLibreNMS < MySQL # rubocop:disable Metrics/ClassLength
    # Print the report.
    def report
      period = Time.now.to_i - @opts['periodicity']
      results = send_query(period)
      data = format_data(results)
      print_to_output(data)
    end

    # Print the result to the selected output
    def print_to_output(data)
      if @opts['output'].nil?
        puts data
      else
        File.write(@opts['output'], data)
      end
    end

    # Send queries to the database
    def send_query(since_period)
      results = {}
      results['hosts'] = id_to_host
      results['total'] = total_host_count
      results['up'] = up_host_count
      results['down'] = down_host_count
      results['average'] = average(since_period)
      results['longests'] = longest(since_period)
      results['missings'] = missing(since_period)
      results
    end

    # Get device_id and hostname matching
    def id_to_host
      q = 'SELECT hostname,device_id FROM devices;'
      Hash[query(q)]
    end

    # Get the total number of hosts.
    def total_host_count
      q = 'SELECT COUNT(*) FROM devices;'
      query(q)[0][0].to_i
    end

    # Get the number of hosts up.
    def up_host_count
      q = 'SELECT COUNT(*) FROM devices WHERE status = 1;'
      query(q)[0][0].to_i
    end

    # Get the number of hosts down.
    def down_host_count
      q = 'SELECT COUNT(*) FROM devices WHERE status = 0;'
      query(q)[0][0].to_i
    end

    # Get the average polling duration.
    def average(since_period)
      q = 'SELECT AVG(duration) FROM perf_times '\
        "WHERE start > '#{since_period}' AND perf_times.type = 'poll';"
      query(q)[0][0].to_i
    end

    # Get the 5 longest poller.
    def longest(since_period)
      q = 'SELECT hostname,MAX(duration) AS drt FROM devices ' \
        'LEFT JOIN perf_times ON devices.device_id=perf_times.doing ' \
        "WHERE start > '#{since_period}' AND perf_times.type = 'poll' " \
        'GROUP BY doing ORDER BY drt DESC LIMIT 5;'
      query(q)
    end

    # Get the missing polling count.
    def missing(since_period)
      q = 'SELECT hostname,start FROM devices LEFT JOIN perf_times ' \
        'ON devices.device_id=perf_times.doing ' \
        "WHERE start > '#{since_period}' AND perf_times.type = 'poll' " \
        'ORDER BY hostname DESC, start ASC;'
      search_for_missing(query(q))
    end

    # Process the data to find the missing polling.
    def search_for_missing(data_array)
      miss_hash = {}
      array_to_hash(data_array).each_pair do |key, array|
        ref = array[0].to_i
        miss_hash[key] = 0
        array.each do |val|
          miss_hash[key] += (val.to_i - ref) / 420 unless val.to_i < ref + 420
          ref = val.to_i
        end
      end
      miss_hash
    end

    # Convert an array to hash.
    def array_to_hash(array)
      myhash = {}
      array.each do |pair|
        myhash[pair[0]] = [] if myhash[pair[0]].nil?
        myhash[pair[0]].push(pair[1])
      end
      myhash
    end

    # Format output to selected datatype.
    def format_data(results)
      case @opts['format'].downcase
      when 'text' then format_text(results)
      when 'json' then format_json(results)
      end
    end

    # Format to json output
    def format_json(res)
      {
        'hosts' => {
          'up' => res['up'], 'down' => res['down'], 'total' => res['total']
        },
        'polling' => {
          'average' => res['average'], 'longest' => Hash[res['longests']],
          'missing' => res['missings'].delete_if { |_, v| v.zero? }
                                      .sort_by { |_, v| v }.reverse
        }
      }.to_json
    end

    # Format to text output.
    def format_text(res)
      out = format_text_hosts(res['up'], res['down'], res['total'])
      out += format_text_average(res['average'])
      out += format_text_longest(res['longests'])
      out += format_text_missing(res['missings'], res['hosts'])
      out
    end

    # Format the text output for host status.
    def format_text_hosts(up, down, total)
      "Number of hosts UP: #{up}/#{total} " \
        "(#{(up.to_f / total * 100).round(2)}%)\n" \
        "Number of hosts DOWN: #{down}/#{total} " \
        "(#{(down.to_f / total * 100).round(2)}%)\n" \
    end

    # Format the text output for average polling.
    def format_text_average(value)
      "Average polling duration: #{value}\n"
    end

    # Format the text output for longest polling list.
    def format_text_longest(values)
      out = "Longest pollers:\n"
      values.each { |v| out += "- #{v[0]} (#{v[1]} seconds)\n" }
      out
    end

    # Format the text output for missing polling list.
    def format_text_missing(values, hosts)
      out = "Missing poll count per host (estimated):\n"
      base = "#{@opts['baseurl'].sub(%r{/^(.*)\/$/}, '\1')}/device/device=" \
        unless @opts['baseurl'].nil?
      Hash[values.sort_by { |_, v| v }.reverse].each_pair do |key, value|
        next if value.zero?
        out += "- #{key}: #{value}"
        out += " (#{base}#{hosts[key]})" unless base.nil?
        out += "\n"
      end
      out
    end
  end
end
